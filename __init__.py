# -*- coding: utf-8 -*-
##############################################################################

from trytond.pool import Pool
from .health_housing import *
from .configuration import *
from .health import *
from .address import *

def register():
  Pool.register(
    GnuHealthSequences,
    HousingConditions,
    DomiciliaryUnit,
    Party,
    Address,    
    GnuHealthConfiguration,
    module='health_housing_conditions_fiuner', type_='model')
